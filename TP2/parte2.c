/**Parte 2: intente modularizar la inicialización de la estructura usando funciones. ¿Qué dificultades o cambios se presentan? 
 * ¿A qué cree que se deben?
 * */
#include <stdio.h>
typedef struct
{
    int velocidadMaxima;
} Automovil;

Automovil setVelocidadMaxima(Automovil autito, int velocidadMaxima)
{
    autito.velocidadMaxima = velocidadMaxima;
    return autito;
}

int main()
{
    Automovil autito;
    autito.velocidadMaxima = 180;
    printf("Velocidad maxima  = %d", autito.velocidadMaxima);
    printf("\n");
    autito = setVelocidadMaxima(autito, 200);
    printf("Velocidad maxima = %d", autito.velocidadMaxima);
    return 0;
}

//Se presenta la dificultad de que es necesario "pisar" el struct anterior para modificar los datos.