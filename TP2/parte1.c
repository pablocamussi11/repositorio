/**
 * TP2: Defina una estructura que represente una entidad en la vida real. 
 * Instancie la estructura en el main, popule los datos e imprímalos. 
 * Parte 2: intente modularizar la inicialización de la estructura usando funciones. ¿Qué dificultades o cambios se presentan? 
 * ¿A qué cree que se deben? 
 * Parte 3: Diseñe una estructura que represente una persona que puede tener hasta 2 hijos. 
 * Con las herramientas vistas hasta ahora ¿qué problemática se presenta en términos de manejo de memoria?
 * */

#include <stdio.h>

typedef struct
{
    int velocidadMaxima;
} Automovil;

int main()
{
    Automovil autito;
    autito.velocidadMaxima = 180;
    printf("Velocidad maxima = %d", autito.velocidadMaxima);
}