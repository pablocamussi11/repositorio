#include <stdio.h>

typedef struct
{
    int valor;
} Estructura;

void setValor(Estructura *estructura)
{
    estructura->valor = 2;
}

int main()
{
    Estructura miEstructura;
    miEstructura.valor = 1;

    setValor(&miEstructura);

    printf("%d", miEstructura.valor);
    return 0;
}