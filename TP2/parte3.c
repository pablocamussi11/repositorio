/** Parte 3: Diseñe una estructura que represente una persona que puede tener hasta 2 hijos. 
 * Con las herramientas vistas hasta ahora ¿qué problemática se presenta en términos de manejo de memoria?
 * */

#include <stdio.h>

typedef struct
{
    char *nombre;
    struct
    {
        char nombre[50];
    } UnHijo;
    struct
    {
        char nombre[50];
    } OtroHijo;

} Persona;

int main()
{
    Persona padre;
    padre->nombre = "Jorge";
    printf("Nombre del padre: %s", padre.nombre);
    return 0;
}

//No es posible modificar el valor del array de caracteres sin usar punteros. Intenté implementarlos pero no tuve éxito
