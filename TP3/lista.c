#include <stdio.h>
#include <stdlib.h>

typedef struct estructuraNodo
{
    int valor;
    struct estructuraNodo *proximo;
} Nodo;

Nodo *agregarElemento(Nodo *lista, int valor)
{
    Nodo *nodoNuevo = malloc(sizeof(Nodo));
    nodoNuevo->valor = valor;

    if (lista == NULL)
    {
        lista = nodoNuevo;
    }
    else
    {
        Nodo *cursor = lista;
        while (cursor->proximo != NULL)
        {
            cursor = cursor->proximo;
        }
        cursor->proximo = nodoNuevo;
    }
    return lista;
}

Nodo *crearLista()
{
    Nodo *lista = malloc(sizeof(Nodo));
    lista = NULL;
    return lista;
}
int main()
{
    Nodo *lista = crearLista();
    lista = agregarElemento(lista, 2);
    lista = agregarElemento(lista, 3);
    lista = agregarElemento(lista, 4);

    printf("%d \n", lista->valor);
    printf("%d \n", lista->proximo->valor);
    printf("%d \n", lista->proximo->proximo->valor);
    return 0;
}