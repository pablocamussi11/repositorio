/**
 * Parte 2:
Implementar una lista de enteros ordenada. 
Cada elemento que agrego queda ordenado en la lista, de manera que al imprimirla se imprime automáticamente ordenada.
*/

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <stdbool.h>

typedef struct estructuraNodo
{
    int valor;
    struct estructuraNodo *proximo;
} Nodo;

void *intercambiar(Nodo *nodoActual, Nodo *nodoSiguiente)
{
    int aux = nodoActual->valor;
    nodoActual->valor = nodoSiguiente->valor;
    nodoSiguiente->valor = aux;
}

Nodo *ordenarLista(Nodo *lista)
{
    bool intercambio;
    Nodo *cursor;
    if (lista != NULL)
    {
        while (intercambio)
        {
            intercambio = false;
            cursor = lista;
            while (cursor->proximo != NULL)
            {
                if (cursor->valor > cursor->proximo->valor)
                {
                    intercambiar(cursor, cursor->proximo);
                    intercambio = true;
                }
                cursor = cursor->proximo;
            }
        }
    }
    return lista;
}

Nodo *agregarElemento(Nodo *lista, int valor)
{
    Nodo *nodoNuevo = malloc(sizeof(Nodo));
    nodoNuevo->valor = valor;
    nodoNuevo->proximo = NULL;

    if (lista == NULL)
    {
        lista = nodoNuevo;
    }
    else
    {
        Nodo *cursor = lista;
        while (cursor->proximo != NULL)
        {
            cursor = cursor->proximo;
        }
        cursor->proximo = nodoNuevo;
    }
    return lista;
}

Nodo *eliminarElemento(Nodo *lista, int posicion)
{
    if (posicion == 1) //quiero eliminar el primero
    {
        Nodo *nodoTemporal = lista;
        lista = lista->proximo;
        free(nodoTemporal);
        nodoTemporal = NULL;
    }
    else if (posicion - 1 == obtenerLargo(lista)) //quiero eliminar el ultimo
    {
        Nodo *cursor = lista;
        for (int i = 0; i < posicion - 2; i++)
        {
            cursor = cursor->proximo;
        }
        free(cursor->proximo);
        cursor->proximo = NULL;
    }
    else //elimino una posicion en el medio
    {
        Nodo *cursor = lista;
        for (int i = 0; i < posicion - 2; i++)
        {
            cursor = cursor->proximo;
        }
        Nodo *nodoTemporal = cursor->proximo;
        cursor->proximo = cursor->proximo->proximo;
        free(nodoTemporal);
        nodoTemporal = NULL;
    }
    return lista;
}

void imprimirLista(Nodo *lista)
{
    Nodo *nodoActual = lista;

    while (nodoActual != NULL)
    {
        printf("%d \n", nodoActual->valor);
        nodoActual = nodoActual->proximo;
    }
}

int consultarElemento(Nodo *lista, int posicion)
{
    assert(posicion <= obtenerLargo(lista)); //Chequeo si la posición pedida es más grande que la lista

    Nodo *nodoActual = lista;

    int contador = 0;

    int valor = 0;

    while (nodoActual != NULL)
    {
        if (contador == posicion - 1)
        {
            valor = nodoActual->valor;
        }
        nodoActual = nodoActual->proximo;
        contador++;
    }
    return valor;
}

int obtenerLargo(Nodo *lista)
{
    int largo = 1;
    Nodo *cursor = lista;

    while (cursor->proximo != NULL)
    {
        largo++;
        cursor = cursor->proximo;
    }
    return largo;
}

Nodo *crearLista()
{
    Nodo *lista = malloc(sizeof(Nodo));
    lista = NULL;
    return lista;
}
int main()
{
    Nodo *lista = crearLista();
    lista = agregarElemento(lista, 2);
    lista = agregarElemento(lista, 3);
    lista = agregarElemento(lista, 7);
    lista = agregarElemento(lista, 5);
    lista = agregarElemento(lista, 1);
    lista = agregarElemento(lista, 9);
    lista = agregarElemento(lista, 8);
    //printf("%d \n", lista->valor);

    //printf("%d \n", obtenerLargo(lista));
    //printf("%d", consultarElemento(lista, 1));

    //lista = eliminarElemento(lista, 3);
    //printf("%d \n", obtenerLargo(lista));

    imprimirLista(lista);

    lista = ordenarLista(lista);

    imprimirLista(lista);
    // printf("%d \n", lista->proximo->valor);
    // printf("%d \n", lista->proximo->proximo->valor);
    return 0;
}
