/**
 * Parte 1:
Hacer la implementación de una lista enlazada.
Debe haber funciones para:
-Crear e inicializar la lista
-Agregar un elemento
-Obtener el largo de la lista
-Obtener un elemento N de la lista
-Eliminar un elemento N de la lista
-Imprimir la lista
*/

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

typedef struct estructuraNodo
{
    int valor;
    struct estructuraNodo *proximo;
} Nodo;

Nodo *agregarElemento(Nodo *lista, int valor)
{
    Nodo *nodoNuevo = malloc(sizeof(Nodo));
    nodoNuevo->valor = valor;
    nodoNuevo->proximo = NULL;

    if (lista == NULL)
    {
        lista = nodoNuevo;
    }
    else
    {
        Nodo *cursor = lista;
        while (cursor->proximo != NULL)
        {
            cursor = cursor->proximo;
        }
        cursor->proximo = nodoNuevo;
    }
    return lista;
}

Nodo *eliminarElemento(Nodo *lista, int posicion)
{
    if (posicion == 1) //quiero eliminar el primero
    {
        Nodo *nodoTemporal = lista;
        lista = lista->proximo;
        free(nodoTemporal);
        nodoTemporal = NULL;
    }
    else if (posicion - 1 == obtenerLargo(lista)) //quiero eliminar el ultimo
    {
        Nodo *cursor = lista;
        for (int i = 0; i < posicion - 2; i++)
        {
            cursor = cursor->proximo;
        }
        free(cursor->proximo);
        cursor->proximo = NULL;
    }
    else //elimino una posicion en el medio
    {
        Nodo *cursor = lista;
        for (int i = 0; i < posicion - 2; i++)
        {
            cursor = cursor->proximo;
        }
        Nodo *nodoTemporal = cursor->proximo;
        cursor->proximo = cursor->proximo->proximo;
        free(nodoTemporal);
        nodoTemporal = NULL;
    }
    return lista;
}

void imprimirLista(Nodo *lista)
{
    Nodo *nodoActual = lista;

    while (nodoActual != NULL)
    {
        printf("%d \n", nodoActual->valor);
        nodoActual = nodoActual->proximo;
    }
}

int consultarElemento(Nodo *lista, int posicion)
{
    assert(posicion <= obtenerLargo(lista)); //Chequeo si la posición pedida es más grande que la lista

    Nodo *nodoActual = lista;

    int contador = 0;

    int valor = 0;

    while (nodoActual != NULL)
    {
        if (contador == posicion - 1)
        {
            valor = nodoActual->valor;
        }
        nodoActual = nodoActual->proximo;
        contador++;
    }
    return valor;
}

int obtenerLargo(Nodo *lista)
{
    int largo = 1;
    Nodo *cursor = lista;

    while (cursor->proximo != NULL)
    {
        largo++;
        cursor = cursor->proximo;
    }
    return largo;
}

Nodo *crearLista()
{
    Nodo *lista = malloc(sizeof(Nodo));
    lista = NULL;
    return lista;
}
int main()
{
    Nodo *lista = crearLista();
    lista = agregarElemento(lista, 2);
    lista = agregarElemento(lista, 3);
    lista = agregarElemento(lista, 7);
    lista = agregarElemento(lista, 5);
    //printf("%d \n", lista->valor);

    //printf("%d \n", obtenerLargo(lista));
    //printf("%d", consultarElemento(lista, 1));

    lista = eliminarElemento(lista, 3);
    //printf("%d \n", obtenerLargo(lista));

    imprimirLista(lista);

    // printf("%d \n", lista->proximo->valor);
    // printf("%d \n", lista->proximo->proximo->valor);
    return 0;
}