#include <stdio.h>

int buscar_maximo(int listado[], int longitud)
{
    int minimo = listado[0];
    int i;
    for (i = 1; i <= longitud; i++)
    {
        if (listado[i] < minimo)
        {
            minimo = listado[i];
        }
    }
    printf("%d", minimo);
}

int main()
{
    int listado[] = {6, 1, 7, 8, 9, 12, 0, -5, 14, -13, 85, 19};
    int longitud = sizeof(listado) / sizeof(listado[0]);
    buscar_maximo(listado, longitud); // Imprime -13
}
