/*
Diseñar un menú navegable donde cada opción muestre una frase
distinta. Debe retornar al menú a menos que se elija la opción “Salir”.
1. Opción 1
2. Opción 2
3. Opción 3
4. Salir
*/

#include <stdio.h>

int mostrarMenu()
{
    int opcion;
    while (opcion != 4)
    {

        printf("1. Opcion 1\n"
               "2. Opcion 2\n"
               "3. Opcion 3\n"
               "4. Salir\n");
        printf("\n");
        printf("Ingrese una opcion: ");
        printf("\n");
        scanf("%d", &opcion);

        if (opcion == 1)
        {
            printf("Se eligio opcion 1");
            printf("\n");
        }
        else if (opcion == 2)
        {
            printf("Se eligio opcion 2");
            printf("\n");
        }
        else if (opcion == 3)
        {
            printf("Se eligio opcion 3");
            printf("\n");
        }
        else if (opcion == 4)
        {
            printf("Se eligio salir");
            printf("\n");
        }
        else
        {
            printf("Ingrese un numero valido");
        }
    }
    return 0;
}
int main()
{
    mostrarMenu();
    return 0;
}