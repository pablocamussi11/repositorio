#include <stdio.h>

int main()
{

    int numero1, numero2, numero3;
    printf("Ingrese tres numeros enteros:");
    scanf("%d %d %d", &numero1, &numero2, &numero3);
    float promedio = (numero1 + numero2 + numero3) / 3.0;
    printf("El promedio de los tres numeros es: %0.2f", promedio); //Imprime el número con 2 decimales

    return 0;
}
