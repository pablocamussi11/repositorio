/*
Imprimir todos los caracteres ASCII.
*/

#include <stdio.h>

int main()
{

    int i;
    char caracter;
    for (i = 0; i < 256; i++)
    {
        printf("%c ", caracter);
        caracter++;
    }

    return 0;
}