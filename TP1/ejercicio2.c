#include <stdio.h>

int buscar_maximo(int listado[], int longitud)
{
    int maximo = listado[0];
    int i;
    for (i = 1; i <= longitud; i++)
    {
        if (listado[i] > maximo)
        {
            maximo = listado[i];
        }
    }
    printf("%d", maximo);
}

int main()
{
    int listado[] = {1, 2, 3, 4, 6, 4, 9, 2, 56, 2, 0, 1, 43};
    int longitud = sizeof(listado) / sizeof(listado[0]);
    buscar_maximo(listado, longitud); // Imprime 56
}
