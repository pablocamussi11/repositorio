/*
Determinar si el número ingresado es par
*/

#include <stdio.h>

int main()
{

    int numero;
    printf("Ingrese un numero: ");
    scanf("%d", &numero);

    if (numero % 2 == 0)
    {
        printf("Es par");
    }
    else
    {
        printf("No es par");
    }

    return 0;
}