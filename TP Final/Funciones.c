#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <stdbool.h>
#include <string.h>
#include "Funciones.h"

#define EDAD_MINIMA 18
#define EDAD_MAXIMA 65
#define MAXIMO_MATERIAS 20
#define MAXIMO_ALUMNOS 15

int legajo = 0;

Estudiante *crearLista()
{
    Estudiante *nodoFicticio = malloc(sizeof(Estudiante));
    Estudiante *lista = malloc(sizeof(Estudiante));
    lista = NULL;
    nodoFicticio->estudianteProximo = lista;
    return nodoFicticio;
}

int obtenerLargoListaEstudiantes(Estudiante *lista)
{
    int tamanio = 0;
    Estudiante *cursor = lista->estudianteProximo;
    if (cursor != NULL)
    {
        while (cursor->estudianteProximo != NULL)
        {
            cursor = cursor->estudianteProximo;
            tamanio++;
        }
        tamanio++;
    }
    return tamanio;
}

int obtenerLargoListaMaterias(Materia *lista)
{
    int tamanio = 0;
    Materia *cursor = lista;
    while (cursor != NULL)
    {
        cursor = cursor->materiaProxima;
        tamanio++;
    }
    return tamanio;
}

int obtenerLargoListaMateriasRendidas(Materia *lista)
{
    int tamanio = 0;
    Materia *cursor = lista;
    while (cursor != NULL)
    {
        if (cursor->nota >= 0)
        {
            tamanio++;
        }
        cursor = cursor->materiaProxima;
    }
    return tamanio;
}

Estudiante *agregarEstudiante(Estudiante *lista, Estudiante *estudianteNuevo)
{
    Estudiante *estudiante = malloc(sizeof(Estudiante));
    estudiante->edad = estudianteNuevo->edad;
    estudiante->nombreEstudiante = estudianteNuevo->nombreEstudiante;
    estudiante->estudianteProximo = NULL;
    estudiante->materias = estudianteNuevo->materias;
    estudiante->legajo = estudianteNuevo->legajo;

    if (lista->estudianteProximo == NULL)
    {

        lista->estudianteProximo = estudiante;
    }
    else
    {
        Estudiante *cabeza = lista;
        while (cabeza->estudianteProximo != NULL && strcmp(cabeza->estudianteProximo->nombreEstudiante, estudiante->nombreEstudiante) < 0)
        {
            cabeza = cabeza->estudianteProximo;
        }
        if (cabeza->estudianteProximo == NULL)
        {
            cabeza->estudianteProximo = estudiante;
        }
        else
        {
            estudiante->estudianteProximo = cabeza->estudianteProximo;
            cabeza->estudianteProximo = estudiante;
        }
    }
    return lista;
}

Estudiante *crearEstudiante(int edad, char *nombreEstudiante, Materia *listaDeMaterias)
{
    if (edad < EDAD_MINIMA || edad > EDAD_MAXIMA)
    {
        printf("Ingrese una edad valida");
        return 0;
    }
    else
    {
        Estudiante *estudianteNuevo = malloc(sizeof(Estudiante));
        estudianteNuevo->edad = edad;
        estudianteNuevo->nombreEstudiante = nombreEstudiante;
        estudianteNuevo->estudianteProximo = NULL;
        estudianteNuevo->materias = listaDeMaterias;
        estudianteNuevo->legajo = legajo + 1;
        legajo = legajo + 1;
        return estudianteNuevo;
    }
}

Estudiante *buscarEstudiantePorLegajo(Estudiante *lista, int legajo)
{
    Estudiante *cursor = lista->estudianteProximo;
    Estudiante *estudiante = malloc(sizeof(Estudiante));
    bool encontro = false;
    while (cursor != NULL)
    {
        if (cursor->legajo == legajo)
        {
            estudiante->edad = cursor->edad;
            estudiante->nombreEstudiante = cursor->nombreEstudiante;
            estudiante->estudianteProximo = NULL;
            estudiante->materias = cursor->materias;
            estudiante->legajo = cursor->legajo;
            encontro = true;
        }
        cursor = cursor->estudianteProximo;
    }
    if (!encontro)
    {
        printf("No se encontro un estudiante con ese legajo");
    }
    return estudiante;
}

Estudiante *eliminarEstudiante(Estudiante *lista, int posicion)
{
    if (posicion == 0)
    {
        Estudiante *nodoTemporal = lista;
        lista = lista->estudianteProximo;
        free(nodoTemporal);
        nodoTemporal = NULL;
    }
    else if (posicion - 1 == obtenerLargoListaEstudiantes(lista))
    {
        Estudiante *cursor = lista->estudianteProximo;
        for (int i = 0; i < posicion - 1; i++)
        {
            cursor = cursor->estudianteProximo;
        }
        free(cursor->estudianteProximo);
        cursor->estudianteProximo = NULL;
    }
    else
    {
        Estudiante *cursor = lista->estudianteProximo;
        for (int i = 0; i < posicion - 1; i++)
        {
            cursor = cursor->estudianteProximo;
        }
        Estudiante *nodoTemporal = cursor->estudianteProximo;
        cursor->estudianteProximo = cursor->estudianteProximo->estudianteProximo;
        free(nodoTemporal);
        nodoTemporal = NULL;
    }
    return lista;
}

void imprimirListaEstudiantes(Estudiante *lista)
{
    Estudiante *estudianteActual = lista->estudianteProximo;
    while (estudianteActual != NULL)
    {
        printf("\nNombre: %s \nLegajo: %i\n", estudianteActual->nombreEstudiante, estudianteActual->legajo);
        estudianteActual = estudianteActual->estudianteProximo;
    }
}

Estudiante *buscarEstudiantesPorNombre(Estudiante *lista, char *nombreDelEstudiante)
{
    Estudiante *cursor = lista->estudianteProximo;
    Estudiante *listaEstudiantes = crearLista();
    bool encontro = false;
    while (cursor != NULL)
    {
        if (strstr(cursor->nombreEstudiante, nombreDelEstudiante))
        {
            listaEstudiantes = agregarEstudiante(listaEstudiantes, cursor);
            encontro = true;
        }
        cursor = cursor->estudianteProximo;
    }
    if (!encontro)
    {
        printf("No se encontro un estudiante con ese nombre");
    }
    return listaEstudiantes;
}

void consultarLegajo(Estudiante *lista, char *nombreDelEstudiante)
{
    imprimirListaEstudiantes(buscarEstudiantesPorNombre(lista, nombreDelEstudiante));
}

void imprimirListaMateriasInscriptas(Estudiante *lista, char *nombreDelEstudiante)
{
    Estudiante *estudiante = buscarEstudiantesPorNombre(lista, nombreDelEstudiante)->estudianteProximo;

    Materia *materiaActual = estudiante->materias;

    while (materiaActual != NULL)
    {
        printf("%s \n", materiaActual->nombreMateria);
        materiaActual = materiaActual->materiaProxima;
    }
}

void imprimirNotasMateriasInscriptas(Estudiante *estudiante)
{
    Materia *materiaActual = estudiante->materias;
    while (materiaActual != NULL)
    {
        if (materiaActual->nota != -1)
        {
            printf("%s: %d \n", materiaActual->nombreMateria, materiaActual->nota);
        }
        else
        {
            printf("%s: %s \n", materiaActual->nombreMateria, "Sin rendir");
        }
        materiaActual = materiaActual->materiaProxima;
    }
}

Estudiante *rendirMateria(Estudiante *lista, char *nombreMateria, int legajo, int nota)
{
    Estudiante *estudianteActual = lista->estudianteProximo;
    bool materiaRendida = false;
    while (estudianteActual != NULL && !materiaRendida)
    {
        if (estudianteActual->legajo == legajo && estudianteActual->materias != NULL)
        {
            Materia *cursor = estudianteActual->materias;
            while (cursor != NULL && !materiaRendida)
            {
                if (cursor->nombreMateria == nombreMateria)
                {
                    cursor->nota = nota;
                    materiaRendida = true;
                }
                cursor = cursor->materiaProxima;
            }
        }
        estudianteActual = estudianteActual->estudianteProximo;
    }
    if (!materiaRendida)
    {
        printf("No se encontro la materia\n");
    }
    return lista;
}

Estudiante *anotarseAMateria(Estudiante *lista, char nombreMateria[], int legajo)
{
    Estudiante *estudianteActual = lista->estudianteProximo;
    bool materiaAgregada = false;
    while (estudianteActual != NULL && !materiaAgregada)
    {
        if (estudianteActual->legajo == legajo)
        {
            Materia *materiaNueva = malloc(sizeof(Materia));
            materiaNueva->nombreMateria = nombreMateria;
            materiaNueva->nota = -1;
            materiaNueva->materiaProxima = NULL;

            if (estudianteActual->materias == NULL)
            {
                estudianteActual->materias = materiaNueva;
            }
            else
            {
                Materia *cursor = estudianteActual->materias;
                while (cursor->materiaProxima != NULL)
                {
                    cursor = cursor->materiaProxima;
                }
                cursor->materiaProxima = materiaNueva;
            }
            materiaAgregada = true;
        }
        estudianteActual = estudianteActual->estudianteProximo;
    }
    return lista;
}

double calcularPromedio(Materia *materias)
{
    Materia *cursor = materias;
    int sumatoriaNotas = 0;
    while (cursor != NULL)
    {
        if (cursor->nota > 0)
        {
            sumatoriaNotas += cursor->nota;
        }
        cursor = cursor->materiaProxima;
    }
    return obtenerLargoListaMateriasRendidas(materias) > 0 ? ((double)sumatoriaNotas / obtenerLargoListaMateriasRendidas(materias)) : 0;
}
int obtenerMateriasAprobadas(Materia *materias)
{
    Materia *cursor = materias;
    int materiasAprobadas = 0;
    while (cursor != NULL)
    {
        if (cursor->nota >= 4)
        {
            materiasAprobadas++;
        }
        cursor = cursor->materiaProxima;
    }
    return materiasAprobadas;
}

void imprimirEstadisticas(Estudiante *lista)
{
    Estudiante *cursor = lista->estudianteProximo;
    printf("|--------|--------------------|-----------------------------------------------------------------|\n");
    printf("| Legajo |       Nombre       |                              Materias\n");
    printf("|--------|--------------------|-----------------------------------------------------------------|\n");
    while (cursor != NULL)
    {
        Materia *materias = cursor->materias;
        int cantidadDeMateriasRendidas = obtenerLargoListaMateriasRendidas(materias);
        int materiasAprobadas = obtenerMateriasAprobadas(materias);
        double promedio = calcularPromedio(materias);
        if (promedio < 0)
        {
            promedio = 0.0;
        }
        int cantidadMateriasAnotado = obtenerLargoListaMaterias(materias);
        printf("Legajo: %d", cursor->legajo);
        printf("|%s:   ||Materias anotado: %d || %d Materias rendidas:||%d Materias aprobadas:|| %.2f Promedio|\n", cursor->nombreEstudiante, cantidadMateriasAnotado, cantidadDeMateriasRendidas, materiasAprobadas, promedio);
        cursor = cursor->estudianteProximo;
    }
}

Estudiante *buscarEstudiantesPorRangoEtario(Estudiante *lista, int rangoInicial, int rangoFinal)
{
    Estudiante *cursor = lista->estudianteProximo;
    Estudiante *listaEstudiantes = crearLista();
    while (cursor != NULL)
    {
        if (cursor->edad <= rangoFinal && cursor->edad > rangoInicial)
        {
            listaEstudiantes = agregarEstudiante(listaEstudiantes, cursor);
        }
        cursor = cursor->estudianteProximo;
    }
    return listaEstudiantes;
}

Estudiante *generarDatosAleatorios(int cantidadDeAlumnos)
{
    char *nombres[15] = {"Pedro", "Pablo", "Martin",
                         "Alejandro", "Braian", "Roberto",
                         "Maria", "Paula", "Ana",
                         "Ramon", "Sol", "Camila",
                         "Manuel", "Andres", "Damian"};
    char *apellidos[15] = {" Perez", " Gomez", " Rodriguez",
                           " Valenzuela", " Godoy", " Alvarez",
                           " Gazano", " Irigoyen", " Camussi",
                           " Pardo", " Vazquez", " Leiva",
                           " Luizaga", " Fernandez", " Martinez"};
    char *materias[15] = {"Algebra I", "Algoritmos y Programacion I", "Analisis Matematico I",
                          "Sistemas Embebidos", "Disenio Logico", "Matematica Discreta",
                          "Cuestiones de Sociologia", "Cultura Contemporanea", "Estructura de Datos",
                          "Fisica I", "Arquitectura de Computadoras", "Matematicas Especiales",
                          "Economia I", "Probabilidad y Estadistica", "Historia I"};

    Estudiante *estudiantes = crearLista();
    Materia *listaMaterias = NULL;
    int materiasAAnotarse;
    int legajoActual;
    char *nombreCompleto;
    for (int i = 0; i < cantidadDeAlumnos; i++)
    {
        nombreCompleto = (char *)malloc(1 + strlen(nombres[rand() % MAXIMO_ALUMNOS]) + strlen(apellidos[rand() % MAXIMO_ALUMNOS]));
        strcpy(nombreCompleto, nombres[rand() % MAXIMO_ALUMNOS]);
        strcat(nombreCompleto, apellidos[rand() % MAXIMO_ALUMNOS]);
        legajoActual = i + 1;
        materiasAAnotarse = rand() % MAXIMO_MATERIAS;
        int edad = rand() % (EDAD_MAXIMA - EDAD_MINIMA + 1) + EDAD_MINIMA;
        estudiantes = agregarEstudiante(estudiantes, crearEstudiante(edad, nombreCompleto, listaMaterias));
        for (int j = 0; j < materiasAAnotarse; j++)
        {
            estudiantes = anotarseAMateria(estudiantes, materias[rand() % MAXIMO_ALUMNOS], legajoActual);
        }
    }
    return estudiantes;
}
