#ifndef FUNCIONES_H_INCLUDED
#define FUNCIONES_H_INCLUDED

typedef struct estructuraEstudiante
{
    struct estructuraMateria *materias;
    int edad;
    char *nombreEstudiante;
    int legajo;
    struct estructuraEstudiante *estudianteProximo;

} Estudiante;

typedef struct estructuraMateria
{
    char *nombreMateria;
    int nota;
    struct estructuraMateria *materiaProxima;
} Materia;

Estudiante *crearLista();

int obtenerLargoListaMaterias(Materia *lista);

int obtenerLargoListaEstudiantes(Estudiante *lista);

int obtenerLargoListaMateriasRendidas(Materia *lista);

Estudiante *agregarEstudiante(Estudiante *lista, Estudiante *estudianteNuevo);

Estudiante *crearEstudiante(int edad, char *nombreEstudiante, Materia *listaDeMaterias);

Estudiante *eliminarEstudiante(Estudiante *lista, int posicion);

Estudiante *buscarEstudiantesPorNombre(Estudiante *lista, char nombreDelEstudiante[]);

Estudiante *buscarEstudiantePorLegajo(Estudiante *lista, int legajo);

void imprimirListaEstudiantes(Estudiante *lista);

void imprimirListaMateriasInscriptas(Estudiante *lista, char nombreDelEstudiante[]);

void imprimirNotasMateriasInscriptas(Estudiante *estudiante);

Estudiante *rendirMateria(Estudiante *lista, char nombreMateria[], int legajo, int nota);

Estudiante *anotarseAMateria(Estudiante *lista, char nombreMateria[], int legajo);

double calcularPromedio(Materia *materias);

int obtenerMateriasAprobadas(Materia *materias);

void imprimirEstadisticas(Estudiante *lista);

Estudiante *buscarEstudiantesPorRangoEtario(Estudiante *lista, int rangoInicial, int rangoFinal);

Estudiante *generarDatosAleatorios(int cantidadDeAlumnos);

#endif
