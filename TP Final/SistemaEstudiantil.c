#include <stdio.h>
#include "Funciones.c"
int main()
{
    Estudiante *lista = crearLista();
    lista = agregarEstudiante(lista, crearEstudiante(21, "Pablo Camussi", NULL));
    lista = agregarEstudiante(lista, crearEstudiante(29, "Damian Perez", NULL));
    lista = agregarEstudiante(lista, crearEstudiante(76, "Ariel Martinez", NULL));

    lista = anotarseAMateria(lista, "Algebra", 1);

    lista = rendirMateria(lista, "Algebra", 1, 3);

    lista = anotarseAMateria(lista, "Analisis", 1);
    lista = rendirMateria(lista, "Analisis", 1, 7);

    imprimirEstadisticas(lista);


    return 0;
}
