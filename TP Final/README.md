# Trabajo Practico Final: Sistema Universitario

## Videos Explicativos:

- [Parte 1](https://www.youtube.com/watch?v=RJrLnXnMKEU)
- [Parte 2](https://www.youtube.com/watch?v=4NBIP0a_Oeo)

## Alumnos:

- [Braian Gazano](https://gitlab.com/BraianGazano)
- [Pablo Camussi](https://gitlab.com/pablocamussi11)

## Ordenamiento de los datos:

El sistema cuenta con orden alfabetico de los nombres, el cual se implementa al momento de agregar un estudiante, el mismo se ubicará en su posicion correspondiente y en el peor de los casos la complejidad de la ubicación será O(N)

## Optativos realizados:

- Generar estudiantes de prueba y materias aleatorias de forma masiva
- Estadísticas de los estudiantes y materias, etc
- Cálculo de promedios.
- Manejo de tipos de entrada del sistema

## Tiempo de procesamiento:

Las pruebas de tiempo consistian en crear, anotar en X cantidad de materias e imprimir estadisticas de los estudiantes.
Intel I5 7400K

- 10.000: 00:01:97 minutos
- 100.000: 00:49.90 minutos
- 200.000: 04:16:04 minutos
- 1.000.000: 15:53:32 minutos

Como podemos ver el sistema comienza a ser insostenible a partir de los 200000 estudiantes.
